#!/usr/bin/env bash

  # # # # # #
  # AlPo.sh # --> a portable shell script which acts as an alias to a directory of other shell scripts; an (Al)ias (Po)rtal.
  # # # # # #

  # Get updates from https://gitlab.com/0ketz/AlPo

# {{{ defs

set -eu -o pipefail;

sub_dir="";
caller_id=${BASH_SOURCE[0]##*/};
script_path=$( realpath ${BASH_SOURCE[0]} | sed s/.sh//g );
script_sub=${script_path##*/};

declare -A ctx=(
    [_src]="$( cd "$( dirname $( realpath "${BASH_SOURCE[0]}" ) )" && pwd)"
    [_exec]="$( pwd )"
);

declare -a subcommands;
cd ${ctx[_src]}${sub_dir};
readarray -d '' subcommands < <(find . -maxdepth 1 -executable -type f -printf "%f\n" | sort )
for s in $subcommands; do if [[ $s =~ $caller_id|$script_sub ]]; then subcommands=( ${subcommands[@]/$s/} ); fi done
subcommands=${subcommands[@]:-}

# }}}

# {{{ funcs

print_help () {
    envsubst <<END
Usage: ${BASH_SOURCE[0]##*/} <subcommand>

  Subcommands:
END

for sub in $subcommands; do
    sub=$(echo $sub | sed s/.sh$//g);
    echo -en "    - $sub\n";
done

[ ${#subcommands[@]} -eq 0 ] && { echo -e "    \e[0;31m- no subcommands available\e[0;0m"; }

echo -e "\n\e[0;30m(powered by AlPo.sh)\e[0;0m";

}

# }}}

# {{{ run

[ $# -gt 0 ] && { # either run the command or show the help, depending on arg count
    true; } || { print_help; exit 1; };

[ "$(printf '%s\n' ${subcommands[@]} | sed s/.sh$//g | grep -P ^$1$ )" != "" ] && { # check if $1 is in $subcommands
    sub=$1; subargs=(${@:2}); cd ${ctx[_exec]};

    . "${ctx[_src]}${sub_dir}/${sub}.sh" "${subargs[@]}"; true; } || { print_help; exit 1; }

# }}}

  # vim: fdm=marker
