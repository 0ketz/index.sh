# `index.sh`: A portal to get onto the `$PATH`.

### What is this?

A portable bash script that's designed to be dropped into an existing directory
and then used via symlink to make a whole directory of shell scripts accessible
to the `$PATH` via subcommands.

### Why does it exist?

I wanted to build up my own personal _stdlib_ of scripts, but have an easy way
to access them from the shell. I store my custom executables in
`$HOME/.local/bin/`, but I don't necessarily want to store all of my scripts in
there, so symlinking is generally required. I find it difficult to manage if
the amount of scripts gets too high, this method allows to cut down on that.

### How does it work?

Well, first you need to have bash installed (luckily most *nix-y systems have
this by default). Presumably you also have a bunch of bash scripts sitting in a
folder somewhere.

1. You **copy** this `index.sh` script into your directory of other scripts.
2. Go to a directory that is on your `$PATH` (typically I use this approach for
   personal scripts, therefore `$HOME/.local/bin/` works for me). Inside this
   directory, create a symbolic link to the `index.sh` script, using whatever
   name you would like to be the _"binary"_.

   `$ ln -s ~/Documents/Source/Project/dotfiles/scripts/index.sh dotscripts`

3. Use the _"binary"_ to execute the other shell scripts that are hidden in the
   directory.
   > e.g: `dotscripts provision-robots`
   >
   > _This will run the
   > `~/Documents/Source/Project/dotfiles/scripts/provision-robots.sh` script._

4. Calling the _"binary"_ without a subcommand will list the available bash
   scripts that there are to run.

### Is it safe to use?

_Probably_, I'm not an expert on writing perfectly secure shell scripts and
there's every chance that mistakes have been made here so use it at your own
risk. It's good enough for me, but make your own assessment.

## FAQ

> I added it to my directory, but it can't find any subcommands.

- It might be worth confirming that your shell scripts are executable, because
  this script will _ONLY_ list executable files as subcommands.

> Everything works well enough, but I'm struggling to use arguments with
> subcommands.

- I'm still working on correctly parsing the arguments for subcommands, this is a
  **TODO**.

> Will there be more features?

- Possibly, but I think this mostly does its job at this stage.

> I want to make it do X

- Awesome, please feel free to grab this and hack it into something else, if you
  think your hacks would be beneficial to upstream then send a pull-request and
  we'll look at it.

---

_made by [ketz](https://twitter.com/0ketz) :cat:_
